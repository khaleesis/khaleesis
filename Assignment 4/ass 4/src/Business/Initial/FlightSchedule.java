/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.sql.Time;

/**
 *
 * @author aboli
 */
public class FlightSchedule {
    int flightNum;
    Time depTime;
    Time arrivalTime;
    int availableWindowSeats[];
    int availableMiddleSeats[];
    int availableAisleSeats[];
    String isCancelled;

    public String getIsCancelled() {
        return isCancelled;
    }

    public void setIsCancelled(String isCancelled) {
        this.isCancelled = isCancelled;
    }

    public int getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(int flightNum) {
        this.flightNum = flightNum;
    }

    public Time getDepTime() {
        return depTime;
    }

    public void setDepTime(Time depTime) {
        this.depTime = depTime;
    }

    public Time getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Time arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int[] getAvailableWindowSeats() {
        return availableWindowSeats;
    }

    public void setAvailableWindowSeats(int[] availableWindowSeats) {
        this.availableWindowSeats = availableWindowSeats;
    }

    public int[] getAvailableMiddleSeats() {
        return availableMiddleSeats;
    }

    public void setAvailableMiddleSeats(int[] availableMiddleSeats) {
        this.availableMiddleSeats = availableMiddleSeats;
    }

    public int[] getAvailableAisleSeats() {
        return availableAisleSeats;
    }

    public void setAvailableAisleSeats(int[] availableAisleSeats) {
        this.availableAisleSeats = availableAisleSeats;
    }
    
    
}
