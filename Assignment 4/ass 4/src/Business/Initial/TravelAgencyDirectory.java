/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import Business.Abstract.User;
import Business.Users.TravelAgency;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aboli
 */
public class TravelAgencyDirectory {
     private List <User> TravelAgencyList;
     

    public TravelAgencyDirectory(){
        TravelAgencyList = new ArrayList<>();
    }
    public List<User> getTravelAgencyList() {
        return TravelAgencyList;
    }

    public void setTravelAgencyList(List<User> TravelAgencyList) {
        this.TravelAgencyList = TravelAgencyList;
    }
     
     public TravelAgency addta(){
        TravelAgency ta = new TravelAgency ();
        TravelAgencyList.add(ta);
        return ta;
    }
     
    
    public void deleteTravelAgency(TravelAgency ta){
        TravelAgencyList.remove(ta);
    }
     
}


