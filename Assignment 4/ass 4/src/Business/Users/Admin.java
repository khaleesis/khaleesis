/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.AirlinerDirectory;
import Business.Initial.TravelAgencyDirectory;

/**
 *
 * @author aboli
 */
public class Admin extends User{
    public TravelAgencyDirectory travelAgencyDir;
    public AirlinerDirectory airlinerDir;
    
    public Admin() {
        super("", "", "Admin");
        airlinerDir = new AirlinerDirectory();
        travelAgencyDir = new TravelAgencyDirectory();
    }

    public TravelAgencyDirectory getTravelAgencyDir() {
        return travelAgencyDir;
    }

    public void setTravelAgencyDir(TravelAgencyDirectory travelAgencyDir) {
        this.travelAgencyDir = travelAgencyDir;
    }

    public AirlinerDirectory getAirlinerDir() {
        return airlinerDir;
    }

    public void setAirlinerDir(AirlinerDirectory airlinerDir) {
        this.airlinerDir = airlinerDir;
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }

}
