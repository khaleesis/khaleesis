/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.AirlinerDirectory;
import Business.Initial.FlightSchedule;
import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class Airliner extends User implements Comparable<Airliner> {
    /*private String flightNum;
    private String flightName;
    private String fromDestination;
    private String toDestination;
    private int price;*/
    
    private FlightSchedule directory;

    public Airliner(String password, String userName) {
        super(password, userName, "Airliner");
        directory = new FlightSchedule();
    }
    
    public FlightSchedule getDirectory() {
        return directory;
    }

    public void setDirectory(FlightSchedule directory) {
        this.directory = directory;
    }

   
    
  
    /*public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    
    public String getFlightNum() {
        return flightNum;
    }

    public void setFlightNum(String flightNum) {
        this.flightNum = flightNum;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFromDestination() {
        return fromDestination;
    }

    public void setFromDestination(String fromDestination) {
        this.fromDestination = fromDestination;
    }

    public String getToDestination() {
        return toDestination;
    }

    public void setToDestination(String toDestination) {
        this.toDestination = toDestination;
    }
    */
    public boolean verify(String password) {
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    @Override
    public int compareTo(Airliner o) {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    
}


