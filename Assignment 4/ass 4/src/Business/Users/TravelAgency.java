/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.AirlinerDirectory;

/**
 *
 * @author aboli
 */
public class TravelAgency extends User implements Comparable<TravelAgency>{
    private String airName;
    private String airType;

    public TravelAgency(){}
    
    public TravelAgency(String airName,String airType){
        this.airName = airName;
        this.airType = airType;
    }

   

    public String getAirName() {
        return airName;
    }

    public void setAirName(String airName) {
        this.airName = airName;
    }

    public String getAirType() {
        return airType;
    }

    public void setAirType(String airType) {
        this.airType = airType;
    }
    
    

    private AirlinerDirectory directory;
    
    public TravelAgency(String password, String userName, String airName, String airType) {
        super(password, userName, "Travel Agency");
        directory = new AirlinerDirectory();
        this.airName = airName;
        this.airType =airType;
    }

    public AirlinerDirectory getDirectory() {
        return directory;
    }

    public void setDirectory(AirlinerDirectory directory) {
        this.directory = directory;
    }

    
    @Override
    public int compareTo(TravelAgency o) {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    /*private String travelAgency;
    private String airlinerName;
    private String airlinerType;

    public String getTravelAgency() {
        return travelAgency;
    }

    public void setTravelAgency(String travelAgency) {
        this.travelAgency = travelAgency;
    }

    public String getAirlinerType() {
        return airlinerType;
    }

    public void setAirlinerType(String airlinerType) {
        this.airlinerType = airlinerType;
    }
 
    
    public String getAirlinerName() {
        return airlinerName;
    }

    public void setAirlinerName(String airlinerName) {
        this.airlinerName = airlinerName;
    } */
    
}
