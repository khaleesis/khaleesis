/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

/**
 *
 * @author aboli
 */
public class Seat {
    
     
    String seatNum;
    boolean isEmpty;
    String seatClass;
    String seatType;  
    int cost;
    
    public Seat(String seatNum,boolean isEmpty, String seatClass,String seatType,int cost)
    {
        this.seatNum= seatNum;   
        this.isEmpty= isEmpty;
        this.seatClass= seatClass;
        this.seatType= seatType;
        this.cost = cost;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getSeatNum() {
        return seatNum;
    }

    public void setSeatNum(String seatNum) {
        this.seatNum = seatNum;
    }

    public boolean getIsEmpty() {
        return isEmpty;
    }

    public void setIsEmpty(boolean isEmpty) {
        this.isEmpty = isEmpty;
    }

    public String getSeatClass() {
        return seatClass;
    }

    public void setSeatClass(String seatClass) {
        this.seatClass = seatClass;
    }

    public String getSeatType() {
        return seatType;
    }

    public void setSeatType(String seatType) {
        this.seatType = seatType;
    }
    
    @Override
    public String toString()
    {
        return seatNum;
    }
}
