/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class AirlineDirectory {
    
    private ArrayList<Airline> AirlinesDirectory;
    
    public AirlineDirectory()
    {
        AirlinesDirectory = new ArrayList<Airline>();
    }


    public ArrayList<Airline> getAirlinesDirectory() {
        return AirlinesDirectory;
    }

    public void setAirlinesDirectory(ArrayList<Airline> AirlinesDirectory) {
        this.AirlinesDirectory = AirlinesDirectory;
    }
    
    public Airline addList()
    {
        Airline addList= new Airline();
        AirlinesDirectory.add(addList);
        return addList;
        
    }
    
    
    
}
