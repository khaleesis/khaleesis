/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class TravelAgency {
    
    private ArrayList<Customer> customerDirectory;
    private ArrayList<Airline> airlineDirectory;
    private ArrayList<Booking> bookingDirectory;
    String name;

    public ArrayList<Booking> getBookingDirectory() {
        return bookingDirectory;
    }

    public void setBookingDirectory(ArrayList<Booking> bookingDirectory) {
        this.bookingDirectory = bookingDirectory;
    }

    public ArrayList<Customer> getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(ArrayList<Customer> customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public ArrayList<Airline> getAirlineDirectory() {
        return airlineDirectory;
    }

    public void setAirlineDirectory(ArrayList<Airline> airlineDirectory) {
        this.airlineDirectory = airlineDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public TravelAgency(String name)
    {
        this.name= name;
        this.customerDirectory= new ArrayList<>();
        this.airlineDirectory= new ArrayList<>();       
    }
    
    public TravelAgency()
    {
        this.name= name;
        this.customerDirectory= new ArrayList<>();
        this.airlineDirectory=new ArrayList<>();
    }
    
}
