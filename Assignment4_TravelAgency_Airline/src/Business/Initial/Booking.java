/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.util.Date;

/**
 *
 * @author aboli
 */
public class Booking {
    
    public enum BookingStatus
    {
        CONFIRMED,
        CANCELLED;
    }
    
    int bookingId=0;
    Seat seat;
    BookingStatus bookingStatus;
    Flight flight;
    Customer customer;
    Date bookingDate;
    
    public Booking(Seat seat,BookingStatus bookingStatus,Flight flight,Customer customer,Date bookingDate)
    {
        bookingId= bookingId++;
        this.seat= seat;
        this.bookingStatus= bookingStatus;
        this.flight = flight;
        this.customer= customer;
        this.bookingDate= bookingDate;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
