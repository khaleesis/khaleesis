/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import Business.Abstract.User;
import Business.Users.UTravelAgency;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aboli
 */
public class UTravelAgencyDirectory {
     private List <User> TravelAgencyList;
     

    public UTravelAgencyDirectory(){
        TravelAgencyList = new ArrayList<>();
    }
    public List<User> getTravelAgencyList() {
        return TravelAgencyList;
    }

    public void setTravelAgencyList(List<User> TravelAgencyList) {
        this.TravelAgencyList = TravelAgencyList;
    }     
    
    public void deleteTravelAgency(UTravelAgency ta){
        TravelAgencyList.remove(ta);
    }
     
}


