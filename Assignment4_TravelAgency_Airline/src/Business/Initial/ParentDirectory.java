/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class ParentDirectory {
    
    private ArrayList parentDirectory;
    private CustomerDirectory customerDirectory;
    private AirlineDirectory airlinesDirectory;

    public ArrayList getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(ArrayList parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public AirlineDirectory getAirlinesDirectory() {
        return airlinesDirectory;
    }

    public void setAirlinesDirectory(AirlineDirectory airlinesDirectory) {
        this.airlinesDirectory = airlinesDirectory;
    }
    
    
    public ParentDirectory()
    {
        parentDirectory = new ArrayList();
        
        this.customerDirectory= new CustomerDirectory();
        this.airlinesDirectory= new AirlineDirectory();
        
        parentDirectory.add(customerDirectory);
        parentDirectory.add(airlinesDirectory);
    }
    
}
