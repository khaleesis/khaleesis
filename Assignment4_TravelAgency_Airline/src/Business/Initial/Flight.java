/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class Flight {
    
    private String flightId;
    private String source;
    private String destination;
    private ArrayList<Seat> seat;
    private Airline airline;
    private String timeoftheday;
    
    public Flight(String flightID, String source, String destination,Airline airline, String timeoftheday) 
    {
        
        this.flightId=flightID;
        //this.capacity=capacity;
        this.source=source;
        this.destination=destination;
        this.airline=airline;
        this.timeoftheday=timeoftheday;
    
    }
    
    Flight(String q123, int i, String bos, String bom) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public String getFlightId() {
        return flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public ArrayList<Seat> getSeat() {
        return seat;
    }

    public void setSeat(ArrayList<Seat> seat) {
        this.seat = seat;
    }

    public Airline getAirline() {
        return airline;
    }

    public void setAirline(Airline airline) {
        this.airline = airline;
    }

    public String getTimeoftheday() {
        return timeoftheday;
    }

    public void setTimeoftheday(String timeoftheday) {
        this.timeoftheday = timeoftheday;
    }
    
    public void removeSeat(Seat seat)
    {
        this.seat.remove(seat);
    }
    
    public void addSeat(Seat seat)
    {
        this.seat.add(seat);
    }
 
}
