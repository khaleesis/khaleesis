/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Initial;

import Business.Abstract.User;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aboli
 */

public class UAirlinerDirectory {
    private List <User> airlinerList;

    public UAirlinerDirectory(){
        airlinerList = new ArrayList<>();
    }
    
    public List<User> getAirlinerList() {
        return airlinerList;
    }

    public void setAirlinerList(List<User> airlinerList) {
        this.airlinerList = airlinerList;
    }  
}
