/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Abstract;

/**
 *
 * @author aboli
 */
public abstract class User {
    private String userName;
    private String password;
    private String role;

    public User(String password, String userName, String role) {
        this.password = password;
        this.userName = userName;
        this.role = role;
       
    }

   public User(){}
   
   public String getAirName(){return null;}
   public String getAirType(){ return null;}
   
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
}
