/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.UAirlinerDirectory;
import java.util.ArrayList;

/**
 *
 * @author aboli
 */
public class UAirliner extends User implements Comparable<UAirliner> {
     
    private UAirlinerDirectory directory;

    public UAirliner(String password, String userName) {
        super(password, userName, "Airliner");
        directory = new UAirlinerDirectory();
    }
    
    public UAirlinerDirectory getDirectory() {
        return directory;
    }

    public void setDirectory(UAirlinerDirectory directory) {
        this.directory = directory;
    }


    public boolean verify(String password) {
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
    @Override
    public int compareTo(UAirliner o) {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    
}


