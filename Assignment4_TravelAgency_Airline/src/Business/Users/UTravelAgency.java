/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.UAirlinerDirectory;
import Business.Initial.UTravelAgencyDirectory;

/**
 *
 * @author aboli
 */
public class UTravelAgency extends User implements Comparable<UTravelAgency>{
   
    private UTravelAgencyDirectory directory;
    
    public UTravelAgency(String password, String userName) {
        super(password, userName, "Travel Agency");
        directory = new UTravelAgencyDirectory();
       
    }

    public UTravelAgencyDirectory getDirectory() {
        return directory;
    }

    public void setDirectory(UTravelAgencyDirectory directory) {
        this.directory = directory;
    }

    
    @Override
    public int compareTo(UTravelAgency o) {
        return o.getUserName().compareTo(this.getUserName());
    }
    
    @Override
    public String toString() {
        return getUserName(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }
    
}
