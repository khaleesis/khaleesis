/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Users;

import Business.Abstract.User;
import Business.Initial.UAirlinerDirectory;
import Business.Initial.UTravelAgencyDirectory;

/**
 *
 * @author aboli
 */
public class Admin extends User{
    public UTravelAgencyDirectory travelAgencyDir;
    public UAirlinerDirectory airlinerDir;
    
    public Admin() {
        super("", "", "Admin");
        airlinerDir = new UAirlinerDirectory();
        travelAgencyDir = new UTravelAgencyDirectory();
    }

    public UTravelAgencyDirectory getTravelAgencyDir() {
        return travelAgencyDir;
    }

    public void setTravelAgencyDir(UTravelAgencyDirectory travelAgencyDir) {
        this.travelAgencyDir = travelAgencyDir;
    }

    public UAirlinerDirectory getAirlinerDir() {
        return airlinerDir;
    }

    public void setAirlinerDir(UAirlinerDirectory airlinerDir) {
        this.airlinerDir = airlinerDir;
    }
    
    public boolean verify(String password){
        if(password.equals(getPassword()))
            return true;
        return false;
    }

}
