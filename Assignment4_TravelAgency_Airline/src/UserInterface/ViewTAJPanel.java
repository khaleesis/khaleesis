/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface;

import Business.Initial.Airline;
import Business.Initial.Flight;
import Business.Initial.Seat;
import Business.Initial.TravelAgency;
import Business.Users.Admin;
import Business.Users.UTravelAgency;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author dhana
 */
public class ViewTAJPanel extends javax.swing.JPanel {
    private JPanel rightJPanel;
    private Flight f;
    Admin admin;
    private Airline a;
    /**
     * Creates new form ViewTAJPanel
     */
    

    ViewTAJPanel(JPanel rightJPanel, Admin admin,Flight f,Airline a) {
        
        initComponents();
        this.rightJPanel = rightJPanel;
        this.f = f;
        this.a =a;
        populate();
        txtAirlineName.setEnabled(false);
        
    }
     private void populate(){
         txtAirlineName.setText(f.getAirline().toString());
         txtSource.setText(f.getSource());
         txtDestination.setText(f.getDestination());
         txtTime.setText(f.getTimeoftheday());
         txtFlightId.setText(f.getFlightId());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnBack = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtFlightId = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtSource = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtDestination = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtTime = new javax.swing.JTextField();
        btnUpdate = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();
        txtAirlineName = new javax.swing.JTextField();

        btnBack.setBackground(new java.awt.Color(0, 51, 153));
        btnBack.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnBack.setForeground(new java.awt.Color(255, 255, 255));
        btnBack.setText("<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Airliner Name:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Flight Id:");

        txtFlightId.setEditable(false);
        txtFlightId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFlightIdActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("View Panel");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Source:");

        txtSource.setEditable(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Destination:");

        txtDestination.setEditable(false);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Time:");

        txtTime.setEditable(false);

        btnUpdate.setBackground(new java.awt.Color(0, 51, 153));
        btnUpdate.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(255, 255, 255));
        btnUpdate.setText("Update");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnSave.setBackground(new java.awt.Color(0, 51, 153));
        btnSave.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnSave.setForeground(new java.awt.Color(255, 255, 255));
        btnSave.setText("Save");
        btnSave.setEnabled(false);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnUpdate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtSource, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFlightId, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtTime, javax.swing.GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
                                    .addComponent(txtDestination)))
                            .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAirlineName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(236, 236, 236)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(278, 291, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(btnBack))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(23, 23, 23)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtAirlineName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtFlightId, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSource, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txtDestination, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTime, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(138, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        CardLayout layout = (CardLayout)rightJPanel.getLayout();
        rightJPanel.remove(this);
        layout.previous(rightJPanel);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
       // System.out.println("Clicked updateb btn");
        btnSave.setEnabled(true);
        btnUpdate.setEnabled(false);
        txtAirlineName.setEditable(false);
        txtDestination.setEditable(true);
        txtFlightId.setEditable(true);
        txtSource.setEditable(true);
        txtTime.setEditable(true);
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        //System.out.println("Clicked save btn");
        btnSave.setEnabled(false);
        btnUpdate.setEnabled(true);
        txtAirlineName.setEditable(false);
        txtDestination.setEditable(false);
        txtFlightId.setEditable(false);
        txtSource.setEditable(false);
        txtTime.setEditable(false);
        
        a.removeFlight(f);
        if(txtFlightId.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Please enter Flight ID..");
            
        }
        
              
        if(txtSource.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Please enter Flight's Source..");
            
        }
        
        if (txtSource.getText().equals("^[a-zA-Z]*$"))
        {
            JOptionPane.showMessageDialog(null, "Please enter only Alphabets");
            
        }
        
        if(txtDestination.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Please enter Flight's Destination..");
            
        }
        
        if (txtDestination.getText().equals("^[a-zA-Z]*$"))
        {
            JOptionPane.showMessageDialog(null, "Please enter only Alphabets");
            
        }
        
        if(txtTime.getText().equals(""))
        {
            JOptionPane.showMessageDialog(null, "Please enter Time of the Day..");
           
        }
        
         
        if(txtTime.getText().equals("[0-9]+"))
        {
            JOptionPane.showMessageDialog(null, "Please enter a numeric value for time");
           
        }
        //String airline = txtAirlineName.getText();
        String dest = txtDestination.getText();
        String flightId = txtFlightId.getText();
        String src = txtSource.getText();
        String time = txtTime.getText();
        
        JOptionPane.showMessageDialog(null, "Airliner updated successfully");
        
        Flight flight = new Flight(flightId, src, dest, a, time);
        generateSeats(flight);
        a.getFlight().add(flight);

    }//GEN-LAST:event_btnSaveActionPerformed

    private void generateSeats(Flight flight)
    {
        Seat s1=new Seat("A1",true,"Business","W",2000);
        Seat s2=new Seat("A2",true,"Business","M",2000);
        Seat s3=new Seat("A3",true,"Business","A",2000);
        Seat s4=new Seat("B1",true,"Business","W",2000);
        Seat s5=new Seat("B2",true,"Business","M",2000);
        Seat s6=new Seat("B3",true,"Business","A",2000);
        Seat s7=new Seat("C1",true,"Economy","W",1000);
        Seat s8=new Seat("C2",true,"Economy","M",1000);
        Seat s9=new Seat("C3",true,"Economy","A",1000);
        Seat s10=new Seat("D1",true,"Economy","W",1000);
        Seat s11=new Seat("D2",true,"Economy","M",1000);
        Seat s12=new Seat("D3",true,"Economy","A",1000);
        
        ArrayList<Seat> totalSeats = new ArrayList<Seat>();
        totalSeats.add(s1);
        totalSeats.add(s2);
        totalSeats.add(s3);
        totalSeats.add(s4);
        totalSeats.add(s5);
        totalSeats.add(s6);
        totalSeats.add(s7);
        totalSeats.add(s8);
        totalSeats.add(s9);
        totalSeats.add(s10);
        totalSeats.add(s11);
        totalSeats.add(s12);
        flight.setSeat(totalSeats);
    }
    
    private void txtFlightIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFlightIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFlightIdActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtAirlineName;
    private javax.swing.JTextField txtDestination;
    private javax.swing.JTextField txtFlightId;
    private javax.swing.JTextField txtSource;
    private javax.swing.JTextField txtTime;
    // End of variables declaration//GEN-END:variables
    
}
