/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aboli
 */
public class AnalysisHelper {
    
    //2. Find top 3 best customers
    public void findTopThreeCustomers()
    {
        Map<Integer,Integer> CustomerProducts = new HashMap<>();
        Map<Integer,Customer> cust = DataStore.getInstance().getCustomers();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
         for(Customer cst : cust.values())
        {
            for(Order order : cst.getOrders())
            {
                Item item = order.getItem();
                //int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    if(prod.getProdId() == item.getProductId())
                    {
                        int productProfit = 0;
                        if(CustomerProducts.containsKey(cst.getCustId()))
                        {
                            productProfit = CustomerProducts.get(cst.getCustId());
                        }
                        productProfit+=(salesPrice - prod.getTargetPrice())*qty;
                        CustomerProducts.put(cst.getCustId(), productProfit);             
                    }
                }
            }
        }  
           System.out.println(CustomerProducts);
        
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(CustomerProducts.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList<Integer> fin = new ArrayList<>(temp.keySet());
        System.out.println("Top 3 best customer (buying higher than target are) : ");
        for(int id=0;id<3;id++)
        {
            System.out.println( cust.get(fin.get(id)));
        } 
        
    }
    
    //3. Find top 3 sales persons(sell higher than target)
    public void findTopThreeSalesPersons()
    {
        Map<Integer,Integer> salesPersonProducts = new HashMap<>();
        Map<Integer,SalesPerson> sps = DataStore.getInstance().getSalesPersons();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        //Map<Integer, List<Order>> spOrder = new HashMap<>();
        
        for(SalesPerson sp : sps.values())
        {
            for(Order order : sp.getOrders())
            {
                Item item = order.getItem();
                //int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    if(prod.getProdId() == item.getProductId())
                    {
                        int productProfit = 0;
                        if(salesPersonProducts.containsKey(sp.getSalesId()))
                        {
                            productProfit = salesPersonProducts.get(sp.getSalesId());
                        }
                        productProfit+=(salesPrice - prod.getTargetPrice())*qty;
                        salesPersonProducts.put(sp.getSalesId(), productProfit);             
                    }
                }
            }
        }     
        System.out.println(salesPersonProducts);
        
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(salesPersonProducts.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList<Integer> fin = new ArrayList<>(temp.keySet());
        System.out.println("Top 3 best Sales Persons (Selling higher than target are) : ");
        for(int id=0;id<3;id++)
        {
            System.out.println( sps.get(fin.get(id)));
        }   
    }
    
    //Total revenue for the year that is above expected target
    public void findTotalRevenueAboveTarget()
    {
        Map<Integer,Integer> salesPersonProfit = new HashMap<>();
        Map<Integer,SalesPerson> sps = DataStore.getInstance().getSalesPersons();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        
        for(SalesPerson sp: sps.values())
        {
            for(Order order : sp.getOrders())
            {
                Item item = order.getItem();
                //int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    if(prod.getProdId() == item.getProductId())
                    {
                        if(salesPrice > prod.getTargetPrice())
                        {
                            int productProfit = 0;
                        
                            if(salesPersonProfit.containsKey(sp.getSalesId()))
                            {
                                productProfit = salesPersonProfit.get(sp.getSalesId());
                            }
                            productProfit+=(salesPrice - prod.getTargetPrice())*qty;
                            salesPersonProfit.put(sp.getSalesId(), productProfit);     
                        }
                    }
                }
            }
        }
        System.out.println("Revenue from each Sales Person: "+salesPersonProfit);
        
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(salesPersonProfit.entrySet()); 
  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList<Integer> fin = new ArrayList<>(temp.values());
        int sum=0;
        for(int i=0;i<fin.size();i++)
        {
            sum+=fin.get(i);
        }
        
        System.out.println("Total revenue for the year that is above expected target is : $"+sum);
    }
}
