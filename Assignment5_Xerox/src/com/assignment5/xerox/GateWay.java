/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author kasai
 */
public class GateWay {
    
    DataReader salesReader;
    DataReader productReader;
    AnalysisHelper helper;
    
    public GateWay() throws IOException {
        DataGenerator generator = DataGenerator.getInstance();
        salesReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }
    
    public static void main(String args[]) throws IOException{
        
        DataGenerator generator = DataGenerator.getInstance();
        GateWay inst = new GateWay();
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        
        DataReader salesReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(salesReader.getFileHeader());
        while((orderRow = salesReader.getNextRow()) != null){
            printRow(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }
        
        inst.readData();
    }
    
    private void readData() throws IOException{
        String[] row;
        while((row = productReader.getNextRow()) != null ){
            generateProduct(row);
        }
        while((row = salesReader.getNextRow()) != null ){
            Item item = generateItem(row);
            Order order = generateOrder(row,item);
            generateSalesPerson(row,order);
            generateCustomer(row, order);
        }
        runAnalysis();
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    
    public void generateCustomer(String[] row,Order order)
    {
        int custId = Integer.parseInt(row[5]);     
        Map<Integer,Customer> customers = DataStore.getInstance().getCustomers();
        
        if(customers.containsKey(custId))
        {
            customers.get(custId).getOrders().add(order);
        }
        else
        {
            Customer customer= new Customer(custId);
            customer.getOrders().add(order);
            customers.put(custId, customer);
        }
    }
    
    public void generateProduct(String[] row)
    {
        int prodId= Integer.parseInt(row[0]);
        Product product= new Product(prodId,Integer.parseInt(row[3]));
        DataStore.getInstance().getProducts().put(prodId, product);
    }
    
    public void generateSalesPerson(String[] row,Order order)
    {
        int salesId = Integer.parseInt(row[4]);
        
        Map<Integer,SalesPerson> salesPersons = DataStore.getInstance().getSalesPersons();
        
        if(salesPersons.containsKey(salesId))
        {
            salesPersons.get(salesId).getOrders().add(order);
        }
        else
        {
            SalesPerson salesPerson = new SalesPerson(salesId);
            salesPerson.getOrders().add(order);
            salesPersons.put(salesId, salesPerson);
        }
    }
    
    public Order generateOrder(String[] row, Item item)
    {
        int orderId = Integer.parseInt(row[0]);
        int supplierId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        
        Order order = new Order(orderId, supplierId, customerId, item);
        return order;
    }
    
    public Item generateItem(String[] row)
    {
        int itemId = Integer.parseInt(row[1]);
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity= Integer.parseInt(row[3]);
        
        Item item = new Item(itemId,productId, salesPrice, quantity);
        DataStore.getInstance().getItems().put(itemId, item);
        
        return item;
    }
    
    //to call functions here
    public void runAnalysis()
    {
        System.out.println("Inside runAnalysis function");
        
        //findTopThreeBestNegitiatedProducts();
        helper.findTopThreeCustomers();
        //helper.findTopThreeSalesPersons();
        helper.findTotalRevenueAboveTarget();
        //
    }
     
}
