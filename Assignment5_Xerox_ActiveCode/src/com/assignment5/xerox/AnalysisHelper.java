/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.entities.Customer;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.SalesPerson;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author aboli
 */
public class AnalysisHelper {
    
    //--------------------------------------1. Find top 3 best negotiated products----------------------------------------------
    public void findTopThreeBestNegotiatedProducts()
    {
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        Map<Integer,Integer> prodsSoldAboveTarget = new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        
        for(Order order: orders.values())
        {
            Item item = order.getItem();
            int prodId = item.getProductId();
            int salesPrice = item.getSalesPrice();
            int qty = item.getQuantity();
            
            for(Product prod: prods.values())
            {
                int bestnegotiated = 0;
                int targetPrice = prod.getTargetPrice();
                if(prodId == prod.getProdId())
                {                  
                    if(salesPrice > prod.getTargetPrice())
                    {
                        if(prodsSoldAboveTarget.containsKey(prod.getProdId()))
                        {
                            bestnegotiated = prodsSoldAboveTarget.get(prod.getProdId());
                        }
                        bestnegotiated+=(salesPrice-targetPrice)*qty;
                        prodsSoldAboveTarget.put(prod.getProdId(), bestnegotiated);     
                    }
                }
            }
        }
        System.out.println(prodsSoldAboveTarget);
                
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(prodsSoldAboveTarget.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList cnt = new ArrayList();
        ArrayList<Integer> fin = new ArrayList<>(temp.keySet());
        System.out.println("Top 3 best Negotiated Products : ");
        
        for(Map.Entry<Integer, Integer> entry:list)
        {
            if(cnt.size()==3)
            {
                break;
            }
            System.out.println("Product Id: "+entry.getKey());
            if(!(cnt.contains(entry.getValue())))
            {
                cnt.add(entry.getValue());
            }
        }           
    }  
    
    //------------------------------2. Finf top 3 best customers(customers who bought above target price)-----------------------------------
    public void findTopThreeCustomers()
    {
       Map<Integer,Integer> CustomerProducts = new HashMap<>();
        Map<Integer,Customer> cust = DataStore.getInstance().getCustomers();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
       
       for(Customer cst : cust.values())
        {
            for(Order order : cst.getOrders())
            {
                Item item = order.getItem();
                //int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    int aboveTarget = 0;
                    if(prod.getProdId() == item.getProductId())
                    {                      
                        if(CustomerProducts.containsKey(cst.getCustId()))
                        {
                            aboveTarget = CustomerProducts.get(cst.getCustId());
                        }
                        aboveTarget+=((Math.abs(salesPrice - prod.getTargetPrice()))*qty);
                        CustomerProducts.put(cst.getCustId(), aboveTarget);             
                    }
                }
            }
        }  
        System.out.println(CustomerProducts);
        
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(CustomerProducts.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList cnt = new ArrayList();
        ArrayList<Integer> fin = new ArrayList<>(temp.keySet());
        System.out.println("Top 3 best customer (buying higher than target are) : ");
        for(Map.Entry<Integer, Integer> entry:list)
        {
            if(cnt.size()==3)
            {
                break;
            }
            System.out.println("Custmer Id: "+entry.getKey());
            if(!(cnt.contains(entry.getValue())))
            {
                cnt.add(entry.getValue());
            }
        }     
    }

    //--------------------------------------3. Find top 3 sales persons(sell higher than target)-------------------------------------------
    public void findTopThreeSalesPersons()
     {
        Map<Integer,Integer> salesPersonProducts = new HashMap<>();
        Map<Integer,SalesPerson> sps = DataStore.getInstance().getSalesPersons();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        //Map<Integer, List<Order>> spOrder = new HashMap<>();
        
        for(SalesPerson sp : sps.values())
        {
            for(Order order : sp.getOrders())
            {
                Item item = order.getItem();
                //int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    int productProfit = 0;
                    if(prod.getProdId() == item.getProductId())
                    {
                        if(salesPersonProducts.containsKey(sp.getSalesId()))
                        {
                            productProfit = salesPersonProducts.get(sp.getSalesId());
                        }
                        productProfit+=((salesPrice - prod.getTargetPrice())*qty);
                        salesPersonProducts.put(sp.getSalesId(), productProfit);             
                    }
                }
            }
        }     
        System.out.println(salesPersonProducts);
        
        // Create a list from elements of HashMap 
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(salesPersonProducts.entrySet()); 
  
        // Sort the list 
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer> >() { 
            public int compare(Map.Entry<Integer, Integer> o1,  
                               Map.Entry<Integer, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList<Integer> fin = new ArrayList<>(temp.keySet());
        ArrayList cnt = new ArrayList();
        System.out.println("Top 3 best Sales Persons (Selling higher than target are) : ");
        for(Map.Entry<Integer, Integer> entry:list)
        {
            if(cnt.size()==3)
            {
                break;
            }
            System.out.println("Sales Person Id: "+entry.getKey());
            if(!(cnt.contains(entry.getValue())))
            {
                cnt.add(entry.getValue());
            }
        }   
    }
    
    //--------------------------------------4. Total revenue for the year that is above expected target--------------------------------------------
    public void findTotalRevenueAboveTarget()
    {
        Map<Integer,Integer> salesPersonProfit = new HashMap<>();
        Map<Integer,SalesPerson> sps = DataStore.getInstance().getSalesPersons();
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        
        for(SalesPerson sp: sps.values())
        {
            for(Order order : sp.getOrders())
            {
                Item item = order.getItem();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                for(Product prod : prods.values())
                {
                    int productProfit = 0;
                    if(prod.getProdId() == item.getProductId())
                    {
                        if(salesPersonProfit.containsKey(sp.getSalesId()))
                        {
                            productProfit = salesPersonProfit.get(sp.getSalesId());
                        }
                        productProfit+=(salesPrice*qty);
                        salesPersonProfit.put(sp.getSalesId(), productProfit);     
                    }
                }
            }
        }
        System.out.println("Revenue from each Sales Person: "+salesPersonProfit);
        
        List<Map.Entry<Integer, Integer> > list = 
               new LinkedList<Map.Entry<Integer, Integer> >(salesPersonProfit.entrySet()); 
  
        HashMap<Integer, Integer> temp = new LinkedHashMap<Integer, Integer>(); 
        for (Map.Entry<Integer, Integer> aa : list) { 
            temp.put(aa.getKey(), aa.getValue()); 
        } 
        ArrayList<Integer> fin = new ArrayList<>(temp.values());
        int sum=0;
        for(int i=0;i<fin.size();i++)
        {
            sum+=fin.get(i);
        }
        
        System.out.println("Total revenue for the year that is above expected target is : $"+sum);
    }


//---------------------5. Determine if the company is pricing its products correctly.Show how to make changes so prices are performing at optimum levels
    
    public void determineOptimumPricing()
    {
        
        Map<Integer,Product> prods = DataStore.getInstance().getProducts();
        Map<Integer,Integer> avgSalesPriceProductSum = new HashMap<>();
        Map<Integer,Integer> avgSalesPriceProductQty = new HashMap<>();
        Map<Integer,Order> orders = DataStore.getInstance().getOrders();
        HashMap<Integer, Float> targetPriceProduct = new LinkedHashMap<Integer, Float>();   
        HashMap<Integer, Float> targetPriceProductCopy = new LinkedHashMap<Integer, Float>();   
        
        for(Product prod : prods.values())
        {
            int targetPrice = prod.getTargetPrice();
            Float ftargetPrice = (float) targetPrice;
            int avgSalesPriceN = 0 , totalQty = 0;
            for(Order order: orders.values())
            {
                Item item = order.getItem();
                int prodId = item.getProductId();
                int salesPrice = item.getSalesPrice();
                int qty = item.getQuantity();
                
                if(prodId == prod.getProdId())
                {     
                    if(avgSalesPriceProductSum.containsKey(prod.getProdId()))
                    {
                        avgSalesPriceN = avgSalesPriceProductSum.get(prod.getProdId());
                        totalQty = avgSalesPriceProductQty.get(prod.getProdId());
                    }
                    avgSalesPriceN+=(salesPrice*qty);
                    totalQty+=qty;
                        
                    avgSalesPriceProductSum.put(prod.getProdId(), avgSalesPriceN);  
                    avgSalesPriceProductQty.put(prod.getProdId(), totalQty);
                }
            }
            targetPriceProduct.put(prod.getProdId(), ftargetPrice);
            targetPriceProductCopy.put(prod.getProdId(), ftargetPrice);
        }
        
        List<Map.Entry<Integer, Integer> > list1 = 
               new LinkedList<Map.Entry<Integer, Integer> >(avgSalesPriceProductSum.entrySet()); 
        List<Map.Entry<Integer, Integer> > list2 = 
               new LinkedList<Map.Entry<Integer, Integer> >(avgSalesPriceProductQty.entrySet()); 
        
        //Get Average Sales Price of each product
        HashMap<Integer, Float> avgSalesPriceProduct = new LinkedHashMap<Integer, Float>(); 
        
        for (Map.Entry<Integer, Integer> aa : list1) 
        {
            for(Map.Entry<Integer,Integer> bb : list2)
            {
                if(aa.getKey()==bb.getKey())
                {
                    Float avgSP = (float)aa.getValue()/bb.getValue();
                    avgSalesPriceProduct.put(aa.getKey(), avgSP); 
                }               
            }           
        }
        
        List<Map.Entry<Integer, Float> > list3 = 
               new LinkedList<Map.Entry<Integer, Float> >(avgSalesPriceProduct.entrySet()); 
        
        //Get Target price of each product
        List<Map.Entry<Integer, Float> > list4 = 
               new LinkedList<Map.Entry<Integer, Float> >(targetPriceProduct.entrySet()); 
  
        //Get AvgSales Price and Target Price Diff
        HashMap<Integer, Float> avgTargetDiff = new LinkedHashMap<Integer, Float>(); 
        
        //Get Error Percentage
        HashMap<Integer, Float> errorPercent = new LinkedHashMap<Integer, Float>(); 
        
        for (Map.Entry<Integer, Float> aa : list3) 
        {
            for(Map.Entry<Integer,Float> bb : list4)
            {
                if(aa.getKey()==bb.getKey())
                {
                    errorPercent.put(aa.getKey(), (bb.getValue()-aa.getValue())*100/aa.getValue());
                    avgTargetDiff.put(aa.getKey(),(aa.getValue()-bb.getValue()));
                }
            }           
        }
        
        List<Map.Entry<Integer, Float> > list5 = 
               new LinkedList<Map.Entry<Integer, Float> >(errorPercent.entrySet());      
        
        List<Map.Entry<Integer, Float> > list6 = 
               new LinkedList<Map.Entry<Integer, Float> >(targetPriceProductCopy.entrySet()); 
  
        float modTP=0.0f;
        
        for (Map.Entry<Integer, Float> i : list5) 
        {
            for(Map.Entry<Integer,Float> j : list6)
            {
                for(Map.Entry<Integer,Float> k : list3)
                {
                   if(i.getKey()==j.getKey() && j.getKey()==k.getKey())
                    {
                        if(i.getValue()<-5.00)
                        {
                            modTP = ((-5*k.getValue())/100)+k.getValue();
                            targetPriceProductCopy.put(i.getKey(),modTP);
                        }
                        else if(i.getValue()>5.00)
                        {
                            modTP = ((5*k.getValue())/100)+k.getValue();
                            targetPriceProductCopy.put(i.getKey(),modTP);
                        }
                    } 
                }
            }               
        }   

        List<Map.Entry<Integer, Float> > sortedAvgTargetDiffList = 
               new LinkedList<Map.Entry<Integer, Float> >(avgTargetDiff.entrySet()); 
  
        // Sort the list 
        Collections.sort(sortedAvgTargetDiffList, new Comparator<Map.Entry<Integer, Float> >() { 
            public int compare(Map.Entry<Integer, Float> o1,  
                               Map.Entry<Integer, Float> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
          
        // put data from sorted list to hashmap  
        HashMap<Integer, Float> temp1 = new LinkedHashMap<Integer, Float>(); 
        for (Map.Entry<Integer, Float> aa : sortedAvgTargetDiffList) { 
            temp1.put(aa.getKey(), aa.getValue()); 
        } 
        
        int negFlagOrig = 0, negFlagMod = 0;
        System.out.println("*******************************************Printing Original Data***************************************************");
        System.out.println("Product Id\tAvg Sales Price\t\tTarget Price\t\tDifference \n"); 
        for(Map.Entry<Integer, Float> diffVal:sortedAvgTargetDiffList)
        {
            for(Map.Entry<Integer, Float> avgSP: list3)
            {
                for(Map.Entry<Integer, Float> tP: list4)
                {
                    if(diffVal.getKey() == avgSP.getKey() && diffVal.getKey() == tP.getKey())
                    {
                        if(diffVal.getValue() < 0 && negFlagOrig == 0)
                        {
                            System.out.println("----------------------------------------------------------------------------------------------------");
                            negFlagOrig = 1;
                        } 
                        System.out.println(diffVal.getKey()+"\t\t"+avgSP.getValue()+"\t\t"+tP.getValue()+"\t\t"+diffVal.getValue());
                    }
                }
            }
        } 

        System.out.println("\n\n*****************************************Printing Modified Data******************************************************");  
        System.out.println("Product Id\tAvg Sales Price\t\tTarget Price\t\tDifference\t\t\tError %\n");

        for(Map.Entry<Integer, Float> diffVal:sortedAvgTargetDiffList)
        {
            for(Map.Entry<Integer, Float> avgSP: list3)
            {
                for(Map.Entry<Integer, Float> mTP: list6)
                {
                    for(Map.Entry<Integer, Float> errorPer: list5)
                    {
                      if(diffVal.getKey() == avgSP.getKey() && diffVal.getKey() == mTP.getKey() && diffVal.getKey() == errorPer.getKey())
                      {
                        if(diffVal.getValue() < 0 && negFlagMod == 0)
                        {
                            System.out.println("-------------------------------------------------------------------------------------------------------------");
                            negFlagMod = 1;
                        }
                        System.out.println(diffVal.getKey()+"\t\t"+avgSP.getValue()+"\t\t"+mTP.getValue()+"\t\t\t"+diffVal.getValue()+"\t\t"+errorPer.getValue());
                      }  
                    }
                    
                }
            }
        }
    }  
}
    