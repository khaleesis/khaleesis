/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Customer {
    int custId;
    private List<Order> orders;

    public Customer(int custId) {
        this.custId = custId;
        this.orders = new ArrayList<>();
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    } 
    
    @Override
    public String toString() {
        return "Customer{" + "customerId = " + custId + ", Number of Orders = " + orders.size() +  '}';
    }
}
