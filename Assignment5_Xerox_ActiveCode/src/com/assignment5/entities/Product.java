/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Product {
    private int productId;
    private int targetPrice;
    private List<Item> items;

    public Product(int productId, int targetPrice) {
        this.productId = productId;
        this.targetPrice = targetPrice;
        this.items = new ArrayList<>();
    }
 
    public int getProdId() {
        return productId;
    }

    public void setProdId(int productId) {
        this.productId = productId;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }
    
    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "User{" + "productId = " + productId + ", targetPrice = " + targetPrice +  '}';
    }
}
